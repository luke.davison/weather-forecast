package net.lukedavison.forecast.utils.ext

import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.showSnackbar(@StringRes stringRes: Int): Snackbar =
        Snackbar.make(findViewById(android.R.id.content), stringRes, Snackbar.LENGTH_INDEFINITE)
                .apply { show() }
