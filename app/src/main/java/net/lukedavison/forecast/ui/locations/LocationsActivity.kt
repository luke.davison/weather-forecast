package net.lukedavison.forecast.ui.locations

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import net.lukedavison.forecast.R
import net.lukedavison.forecast.di.ViewModelFactory
import net.lukedavison.forecast.ui.addlocation.AddLocationActivity
import net.lukedavison.forecast.ui.forecast.ForecastActivity
import net.lukedavison.forecast.ui.forecast.ForecastActivity.Companion.ARG_LOCATION

class LocationsActivity : AppCompatActivity() {
    companion object {
        private const val PERM_REQUEST = 1
        private const val ADD_REQUEST = 2

        const val ARG_CITY = "arg:city"
        const val ARG_STATE = "arg:state"
        const val ARG_COUNTRY = "arg:country"
    }

    private lateinit var viewModel: LocationsViewModel
    private lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locations)
        setSupportActionBar(findViewById(R.id.toolbar))

        fab = findViewById(R.id.fab)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERM_REQUEST)
        }

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view).apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = LocationsAdapter { location ->
                startActivity(Intent(applicationContext, ForecastActivity::class.java)
                        .putExtra(ARG_LOCATION, location))
            }
        }

        val noLocationsImage = findViewById<ImageView>(R.id.no_locations_image)
        val noLocationsText = findViewById<TextView>(R.id.no_locations_text)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this))
                .get(LocationsViewModel::class.java)
        viewModel.locations().observe(this, Observer { locations ->
            locations?.let {
                (recyclerView.adapter as LocationsAdapter).replaceItems(locations)
                if (locations.isNotEmpty()) {
                    noLocationsImage.visibility = GONE
                    noLocationsText.visibility = GONE
                } else {
                    noLocationsImage.visibility = VISIBLE
                    noLocationsText.visibility = VISIBLE
                }
            }
        })

        initFab()
    }

    private fun initFab() {
        fab.apply {
            setOnClickListener { _ ->
                startActivityForResult(
                        Intent(this@LocationsActivity, AddLocationActivity::class.java),
                        ADD_REQUEST)
            }
            show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERM_REQUEST && grantResults.isNotEmpty()
                && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            initFab()
        } else {
            Toast.makeText(this, R.string.location_permission_required, Toast.LENGTH_LONG).show()
            fab.hide()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADD_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val city = data.getStringExtra(ARG_CITY)
            val state = data.getStringExtra(ARG_STATE)
            val country = data.getStringExtra(ARG_COUNTRY)
            viewModel.addLocation(city, state, country)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
