package net.lukedavison.forecast.ui.addlocation

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import net.lukedavison.forecast.R
import net.lukedavison.forecast.di.ViewModelFactory
import net.lukedavison.forecast.ui.locations.LocationsActivity.Companion.ARG_CITY
import net.lukedavison.forecast.ui.locations.LocationsActivity.Companion.ARG_COUNTRY
import net.lukedavison.forecast.ui.locations.LocationsActivity.Companion.ARG_STATE
import net.lukedavison.forecast.utils.ext.showSnackbar
import timber.log.Timber

class AddLocationActivity : AppCompatActivity() {
    companion object {
        private const val PERM_REQUEST = 1
    }

    private lateinit var mapView: MapView
    private lateinit var mapboxMap: MapboxMap
    private lateinit var viewModel: AddLocationViewModel
    private var snackbar: Snackbar? = null
    private var selectAction: MenuItem? = null
    private var marker = Marker(MarkerOptions())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_add_location)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_close_white_24dp)
            setTitle(R.string.title_add_location)
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERM_REQUEST)
        } else {
            getLocation()
        }

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this))
                .get(AddLocationViewModel::class.java)
        viewModel.location.observe(this, Observer { location ->
            marker.title = location?.title() ?: getString(R.string.loading)

            if (marker.isInfoWindowShown)
                marker.infoWindow?.update() else marker.showInfoWindow(mapboxMap, mapView)

            location?.let {
                setResult(Activity.RESULT_OK, Intent()
                        .putExtra(ARG_CITY, location.city)
                        .putExtra(ARG_STATE, location.state)
                        .putExtra(ARG_COUNTRY, location.country))
            }
        })
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                mapboxMap.removeMarker(marker)
                snackbar = showSnackbar(errorMessage)
            } else {
                snackbar?.dismiss()
            }
        })

        mapView = findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync {
            mapboxMap = it
            mapboxMap.addOnMapClickListener { point ->
                mapboxMap.removeMarker(marker)
                marker = mapboxMap.addMarker(MarkerOptions().apply { position = point })
                viewModel.setPosition(point)
            }

            getLocation()?.let { point ->
                mapboxMap.cameraPosition = CameraPosition.Builder()
                        .target(point)
                        .zoom(11.0)
                        .build()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select, menu)
        selectAction = menu?.findItem(R.id.select_action)
        viewModel.selectEnabled.observe(this, Observer { value ->
            selectAction?.isEnabled = value ?: false
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
                return true
            }
            R.id.select_action -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERM_REQUEST && grantResults.isNotEmpty()
                && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            val point = getLocation()
            if (::mapboxMap.isInitialized && point != null) {
                mapboxMap.cameraPosition = CameraPosition.Builder()
                        .target(point)
                        .zoom(11.0)
                        .build()
            }
        }
    }

    // TODO: replace with Fuse Location from Play Services
    private fun getLocation(): LatLng? {
        val locationManager = applicationContext.getSystemService(
                Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)?.let { location ->
                return LatLng(location.latitude, location.longitude, location.altitude)
            }
        } catch (e: SecurityException) {
            Timber.e(e, "Unable to fetch location")
        }
        return null
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState!!)
    }
}
