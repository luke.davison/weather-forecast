package net.lukedavison.forecast.ui.forecast

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.widget.ProgressBar
import net.lukedavison.forecast.R
import net.lukedavison.forecast.data.db.model.Location
import net.lukedavison.forecast.di.ViewModelFactory

class ForecastActivity : AppCompatActivity() {
    companion object {
        const val ARG_LOCATION = "arg:location"
    }

    private lateinit var viewModel: ForecastViewModel
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forecast)

        val location = (savedInstanceState ?: intent.extras)?.getParcelable<Location>(ARG_LOCATION)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title = location?.title() ?: getString(R.string.title_activity_location)
        }

        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
                .apply { adapter = ForecastAdapter() }

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this))
                .get(ForecastViewModel::class.java)
                .apply { location?.let { setLocation(location) } }

        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
        viewModel.forecastDays.observe(this, Observer { forecastDays ->
            forecastDays?.let {
                (recyclerView.adapter as ForecastAdapter).replaceItems(forecastDays)
            }
        })
        viewModel.loadingVisibility.observe(this, Observer { visibility ->
            visibility?.let { progressBar.visibility = visibility }
        })
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(
                findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }
}