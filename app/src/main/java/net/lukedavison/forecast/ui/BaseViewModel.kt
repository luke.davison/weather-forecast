package net.lukedavison.forecast.ui

import androidx.lifecycle.ViewModel
import net.lukedavison.forecast.di.component.DaggerViewModelInjector
import net.lukedavison.forecast.di.module.ApiModule
import net.lukedavison.forecast.ui.addlocation.AddLocationViewModel
import net.lukedavison.forecast.ui.forecast.ForecastViewModel
import net.lukedavison.forecast.ui.locations.LocationsViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector = DaggerViewModelInjector
            .builder()
            .apiModule(ApiModule)
            .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is LocationsViewModel -> injector.inject(this)
            is AddLocationViewModel -> injector.inject(this)
            is ForecastViewModel -> injector.inject(this)
            else -> IllegalArgumentException("Do not know how to inject view model")
        }
    }
}