package net.lukedavison.forecast.ui.locations

import androidx.lifecycle.LiveData
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import net.lukedavison.forecast.data.db.WeatherDao
import net.lukedavison.forecast.data.db.model.Location
import net.lukedavison.forecast.ui.BaseViewModel

class LocationsViewModel(private val dao: WeatherDao) : BaseViewModel() {
    private var subscription: Disposable? = null

    fun locations(): LiveData<List<Location>> = dao.allLocations()

    fun addLocation(city: String, state: String, country: String) {
        subscription = Observable.fromCallable {
            dao.insertLocation(Location(city, state, country))
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }
}