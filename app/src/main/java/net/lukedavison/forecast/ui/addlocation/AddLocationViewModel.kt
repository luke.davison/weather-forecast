package net.lukedavison.forecast.ui.addlocation

import androidx.lifecycle.MutableLiveData
import com.mapbox.mapboxsdk.geometry.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import net.lukedavison.forecast.BuildConfig
import net.lukedavison.forecast.R
import net.lukedavison.forecast.data.api.LocationJson
import net.lukedavison.forecast.data.api.WeatherApi
import net.lukedavison.forecast.ui.BaseViewModel
import javax.inject.Inject

class AddLocationViewModel : BaseViewModel() {
    @Inject
    lateinit var api: WeatherApi
    private var subscription: Disposable? = null

    val location = MutableLiveData<LocationJson>()
    val errorMessage = MutableLiveData<Int>()
    val selectEnabled = MutableLiveData<Boolean>()

    fun setPosition(point: LatLng) {
        subscription = api
                .getCityState(BuildConfig.WUNDERGROUND_API_KEY, point.latitude, point.longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    location.value = null
                    errorMessage.value = null
                    selectEnabled.value = false
                }
                .subscribe(
                        { result ->
                            location.value = result.location
                            selectEnabled.value = true
                        },
                        { errorMessage.value = R.string.fetch_error }
                )
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }
}