package net.lukedavison.forecast.ui.forecast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.lukedavison.forecast.R
import net.lukedavison.forecast.data.db.model.ForecastDay

class ForecastAdapter : RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_forecast_day, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = if (::items.isInitialized) items.size else 0

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
            viewHolder.bind(items[position])

    fun replaceItems(items: List<ForecastDay>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    private lateinit var items: MutableList<ForecastDay>

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title = view.findViewById<TextView>(R.id.title_text)
        private val precip = view.findViewById<TextView>(R.id.precip_text)
        private val description = view.findViewById<TextView>(R.id.description_text)
        private val icon = view.findViewById<ImageView>(R.id.weather_image)

        fun bind(item: ForecastDay) {
            title.text = item.title
            precip.text = itemView.context.getString(R.string.precip, item.probabilityOfPrecip)
            description.text = item.description
            // TODO: fix, just a quick hack to force https over http to get around Pie restrictions
            Glide.with(itemView.context)
                    .load(item.iconUrl.replace("http://", "https://"))
                    .into(icon)
        }
    }
}