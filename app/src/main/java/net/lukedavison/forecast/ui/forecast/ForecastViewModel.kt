package net.lukedavison.forecast.ui.forecast

import androidx.lifecycle.MutableLiveData
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import net.lukedavison.forecast.BuildConfig
import net.lukedavison.forecast.R
import net.lukedavison.forecast.data.api.WeatherApi
import net.lukedavison.forecast.data.db.WeatherDao
import net.lukedavison.forecast.data.db.model.ForecastDay
import net.lukedavison.forecast.data.db.model.Location
import net.lukedavison.forecast.ui.BaseViewModel
import javax.inject.Inject

class ForecastViewModel(private val dao: WeatherDao) : BaseViewModel() {
    @Inject
    lateinit var api: WeatherApi
    private lateinit var subscription: Disposable
    private lateinit var location: Location

    val loadingVisibility = MutableLiveData<Int>()
    val errorMessage = MutableLiveData<Int>()
    val errorClickListener = View.OnClickListener { fetch() }
    val forecastDays = MutableLiveData<List<ForecastDay>>()

    fun setLocation(location: Location) {
        this.location = location
        fetch()
    }

    private fun fetch() {
        subscription = Observable.fromCallable { dao.forecastByLocation(location.id) }
                .concatMap { dbItems ->
                    // TODO: add location.updated_at and allow API call once per 24 hours
                    if (dbItems.isEmpty())
                        api.getForecast(BuildConfig.WUNDERGROUND_API_KEY,
                                location.city, location.state, location.country)
                                .concatMap { response ->
                                    dao.deleteForecastByLocation(location.id)
                                    val apiItems = response.forecast
                                            .textForecast
                                            .forecastByDay
                                            .map { ForecastDay.fromApi(location.id, it) }
                                    dao.insertAllForecastDays(apiItems)
                                    Observable.just(apiItems)
                                }
                    else
                        Observable.just(dbItems)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    loadingVisibility.value = View.VISIBLE
                    errorMessage.value = null
                }
                .doOnTerminate { loadingVisibility.value = View.GONE }
                .subscribe(
                        { result -> forecastDays.value = result },
                        { errorMessage.value = R.string.fetch_error }
                )
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}