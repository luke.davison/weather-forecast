package net.lukedavison.forecast.ui.locations

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.lukedavison.forecast.R
import net.lukedavison.forecast.data.db.model.Location

class LocationsAdapter(private val listener: (location: Location) -> Unit) :
        RecyclerView.Adapter<LocationsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_location, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = if (::items.isInitialized) items.size else 0

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position], listener)
    }

    fun replaceItems(items: List<Location>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    private lateinit var items: MutableList<Location>

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val location = view.findViewById<TextView>(R.id.location_text)

        fun bind(item: Location, listener: (location: Location) -> Unit) {
            location.text = itemView.context.getString(
                    R.string.location, item.city, item.state, item.country)
            itemView.setOnClickListener { listener.invoke(item) }
        }
    }
}