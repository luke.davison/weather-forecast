package net.lukedavison.forecast.data.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherApi {
    @GET("/api/{key}/forecast10day/q/{country}/{state}/{city}.json")
    fun getForecast(@Path("key") apiKey: String,
                    @Path("city") city: String,
                    @Path("state") state: String,
                    @Path("country") country: String): Observable<ForecastResponseJson>

    // NOTE: could have also used MapBox API:
    // https://api.mapbox.com/geocoding/v5/mapbox.places/longitude,latitude.json?access_token=
    @GET("/api/{key}/geolookup/q/{latitude},{longitude}.json")
    fun getCityState(@Path("key") apiKey: String,
                     @Path("latitude") latitude: Double,
                     @Path("longitude") longitude: Double): Observable<GeolookupResponseJson>
}