package net.lukedavison.forecast.data.api

import com.squareup.moshi.Json

/* Forecast API models */
data class ForecastResponseJson(val forecast: ForecastJson)

data class ForecastJson(@field:Json(name = "txt_forecast")
                        val textForecast: TextForecastJson)

data class TextForecastJson(@field:Json(name = "forecastday")
                            val forecastByDay: List<DayJson>)

data class DayJson(val title: String,
                   @field:Json(name = "icon_url")
                   val iconUrl: String,
                   @field:Json(name = "fcttext")
                   val description: String,
                   @field:Json(name = "pop")
                   val probabilityOfPrecip: String)

/* Geolookup API models */
data class GeolookupResponseJson(val location: LocationJson)

data class LocationJson(val country: String,
                        val city: String,
                        val state: String) {
    fun title(): String = "$city, $state, $country"
}

