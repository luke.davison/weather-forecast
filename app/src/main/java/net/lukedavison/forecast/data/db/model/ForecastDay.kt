package net.lukedavison.forecast.data.db.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import net.lukedavison.forecast.data.api.DayJson

@Entity(foreignKeys = [
    ForeignKey(entity = Location::class,
            parentColumns = ["id"],
            childColumns = ["locationId"],
            onDelete = ForeignKey.CASCADE)],
        indices = [Index("locationId")])
data class ForecastDay(@field:PrimaryKey(autoGenerate = true)
                       val id: Int = 0,
                       val locationId: Int,
                       val title: String,
                       val description: String,
                       val iconUrl: String,
                       val probabilityOfPrecip: String) {
    companion object {
        @JvmStatic
        fun fromApi(locationId: Int, dayJson: DayJson): ForecastDay =
                with(dayJson) {
                    ForecastDay(locationId = locationId,
                            title = title,
                            description = description,
                            iconUrl = iconUrl,
                            probabilityOfPrecip = probabilityOfPrecip)
                }
    }
}
