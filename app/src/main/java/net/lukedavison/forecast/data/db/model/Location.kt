package net.lukedavison.forecast.data.db.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity
data class Location(@field:PrimaryKey(autoGenerate = true)
                    val id: Int = 0,
                    val city: String,
                    val state: String,
                    val country: String) : Parcelable {
    @Ignore
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString().orEmpty(),
            parcel.readString().orEmpty(),
            parcel.readString().orEmpty())

    @Ignore
    constructor(city: String, state: String, country: String) : this(0, city, state, country)

    fun title(): String = "$city, $state, $country"

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(country)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }
}