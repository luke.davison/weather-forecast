package net.lukedavison.forecast.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import net.lukedavison.forecast.data.db.model.ForecastDay
import net.lukedavison.forecast.data.db.model.Location

@Database(entities = [Location::class, ForecastDay::class], version = 1)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
}
