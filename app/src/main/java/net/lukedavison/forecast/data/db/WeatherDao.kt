package net.lukedavison.forecast.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import net.lukedavison.forecast.data.db.model.ForecastDay
import net.lukedavison.forecast.data.db.model.Location

@Dao
interface WeatherDao {
    @Query("SELECT * FROM location ORDER BY city, state ASC")
    fun allLocations(): LiveData<List<Location>>

    @Insert
    fun insertLocation(location: Location)

    @Delete
    fun deleteLocation(location: Location)

    @Insert
    fun insertAllForecastDays(forecastDays: List<ForecastDay>)

    @Query("DELETE FROM forecastday WHERE locationId = :locationId")
    fun deleteForecastByLocation(locationId: Int)

    @Query("SELECT * FROM forecastday WHERE locationId = :locationId")
    fun forecastByLocation(locationId: Int): List<ForecastDay>
}