package net.lukedavison.forecast

import android.app.Application
import com.mapbox.mapboxsdk.Mapbox
import timber.log.Timber

class WeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        // TODO: otherwise log to crash reporting tool

        Mapbox.getInstance(this, BuildConfig.MAPBOX_API_KEY)
    }
}