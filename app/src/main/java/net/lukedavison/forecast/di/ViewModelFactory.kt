package net.lukedavison.forecast.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import androidx.appcompat.app.AppCompatActivity
import net.lukedavison.forecast.data.db.WeatherDatabase
import net.lukedavison.forecast.ui.addlocation.AddLocationViewModel
import net.lukedavison.forecast.ui.forecast.ForecastViewModel
import net.lukedavison.forecast.ui.locations.LocationsViewModel

class ViewModelFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val db = Room.databaseBuilder(
                activity.applicationContext, WeatherDatabase::class.java, "weather")
                .build()

        return when {
            modelClass.isAssignableFrom(LocationsViewModel::class.java) ->
                LocationsViewModel(db.weatherDao()) as T
            modelClass.isAssignableFrom(ForecastViewModel::class.java) ->
                ForecastViewModel(db.weatherDao()) as T
            modelClass.isAssignableFrom(AddLocationViewModel::class.java) ->
                AddLocationViewModel() as T
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}