package net.lukedavison.forecast.di.component

import dagger.Component
import net.lukedavison.forecast.di.module.ApiModule
import net.lukedavison.forecast.ui.addlocation.AddLocationViewModel
import net.lukedavison.forecast.ui.forecast.ForecastViewModel
import net.lukedavison.forecast.ui.locations.LocationsViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class])
interface ViewModelInjector {
    fun inject(viewModel: AddLocationViewModel)
    fun inject(viewModel: ForecastViewModel)
    fun inject(viewModel: LocationsViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun apiModule(apiModule: ApiModule): Builder
    }
}