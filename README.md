# Weather Forecast

Use Mapbox to add one or more locations. View a location to see the 10 day forecast from
Weather Underground.

## Setup

Create the `gradle.properties` file in the project root, and setup the API keys for Mapbox and
Weather Underground:

```
mapbox_api_key=
wunderground_api_key=
```
